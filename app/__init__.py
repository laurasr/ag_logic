import sys, pkgutil, importlib, inspect
import logging
import logging.handlers
from flask import Flask, jsonify, Blueprint
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_mail import Mail
from flask import Flask
from flask_security import Security, SQLAlchemyUserDatastore

from flask_wtf.csrf import CSRFProtect

from flask_restx import Api, Resource
from flask_marshmallow import Marshmallow


# from werkzeug.middleware.proxy_fix import ProxyFix


app = Flask(__name__)
# app = ProxyFix(app, x_for=1, x_host=1)
app.config.from_object(Config)


api_blueprint = Blueprint('API_V1', __name__, url_prefix='/api_v1')

api = Api(api_blueprint, doc='/docs', title='API v1')

ma = Marshmallow(app)

mail = Mail(app)

db = SQLAlchemy(app)

login = LoginManager(app)

csrf = CSRFProtect(app)

from app import helpers

app.jinja_env.filters['datetime'] = helpers.format_datetime
app.jinja_env.filters['humanFileSize'] = helpers.format_bytes


class ReverseProxied(object):
	'''Wrap the application in this middleware and configure the 
	front-end server to add these headers, to let you quietly bind 
	this to a URL other than / and to an HTTP scheme that is 
	different than what is used locally.
	In nginx:
	location /myprefix {
		proxy_pass http://192.168.0.1:5001;
		proxy_set_header Host $host;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header X-Scheme $scheme;
		proxy_set_header X-Script-Name /myprefix;
		}
	:param app: the WSGI application
	'''
	def __init__(self, app):
		self.app = app

	def __call__(self, environ, start_response):
		# environ['wsgi.url_scheme'] = 'https'
		script_name = environ.get('HTTP_X_SCRIPT_NAME', '')
		if script_name:
			environ['SCRIPT_NAME'] = script_name
			path_info = environ['PATH_INFO']
			if path_info.startswith(script_name):
				environ['PATH_INFO'] = path_info[len(script_name):]

		scheme = environ.get('HTTP_X_SCHEME', '')
		if scheme:
			environ['wsgi.url_scheme'] = scheme
		return self.app(environ, start_response)

app.wsgi_app = ReverseProxied(app.wsgi_app)


def import_models():                                                           
    thismodule = sys.modules[__name__]                                         

    for loader, module_name, is_pkg in pkgutil.iter_modules(                   
            thismodule.__path__, thismodule.__name__ + '.'):                   
        module = importlib.import_module(module_name, loader.path)             
        for name, _object in inspect.getmembers(module, inspect.isclass):      
            globals()[name] = _object                                                                                     

import_models()  



from app.models import User, Role
from app.forms import ExtendedLoginForm

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore, login_form=ExtendedLoginForm)


from app import routes, models