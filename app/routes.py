import json
import os
import re
import uuid
from flask import abort, render_template, flash, redirect, Response, url_for, request, send_from_directory
from flask_login import login_user, logout_user, current_user, login_required
from flask_mail import Mail, Message
from flask_security import roles_required, roles_accepted, utils as fs_utils
from werkzeug.urls import url_parse

from app import app, api_blueprint, db, mail, ma
from app.forms import LoginForm, UserForm, RegistrationForm
from wtforms.validators import DataRequired
from app import User, Role, UserRoles

from datetime import datetime

#libraries for mapping
import folium
from ipyleaflet import Map, Marker, LayersControl, ScaleControl, DrawControl


@api_blueprint.before_request
def restrict_to_logged_in():
    if not current_user.is_authenticated:
         abort(403)

app.register_blueprint(api_blueprint)



@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
        'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/')
@app.route('/index')
@login_required
def index():
    return render_template('index.html',
        title = 'Forico Planter Admin',
        # authkey = current_user.authkey.hex,
        role = current_user.roles[0])



@app.route('/admin')
@roles_required('Admin')    # Use of @roles_required decorator
def admin_page():
    return render_template('admin.html', title='Admin')


@app.route('/register_user', methods=['GET', 'POST'])
# @roles_accepted('Admin')
def register_user():
    form = UserForm(mode='Add')

    form.password.validators.append(DataRequired())

    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        
        selectedRole = Role.query.filter_by(id=form.role.data.id).first_or_404()
        # if current_user.is_admin():
            # selectedRole = Role.query.filter_by(id=form.role.data.id).first_or_404()
        # elif current_user.is_PAHSMA():
        #     selectedRole = Role.query.filter_by(id=4).first_or_404()

        if len(user.roles) == 0:
            user.roles.append(selectedRole)
        elif user.roles[0] != selectedRole:
            user.roles[0] = selectedRole

        user.active = True
        if not user.authkey:
            user.authkey = uuid.uuid4()
        print(user.authkey)
        db.session.add(user)
        db.session.commit()
        # flash('Congratulations, you are now a registered user!')
        return redirect(url_for('users'))
    return render_template('user.html',
        #title='Register User',
        form=form,
        action='Add')


@app.route('/users', methods=['GET'])
@login_required
@roles_accepted('Admin')
def users():
    if current_user.is_admin():
        users = User.query.order_by('username')
    elif current_user.is_PAHSMA():
        # users = User.query.order_by('username')
        users = User.query.filter(User.roles.any(name='PAHSMA_STAFF')).order_by('username').all()

    return render_template('user_index.html', title='Users', users=users)


@app.route('/user/<username>', methods=['GET', 'POST'])
@login_required
def user(username):
    if current_user.is_admin():
        user = User.query.filter_by(username=username).first_or_404()
    # elif current_user.is_PAHSMA():

    #     user = User.query.filter(User.roles.any(name='PAHSMA_STAFF')).filter_by(username=username).order_by('username').first()

    #     if(user is None):
    #         user = current_user    
    else:
        user = current_user

    form = UserForm(formdata=request.form, obj=user, mode='Edit', role=user.roles[0])

    if form.validate_on_submit():
        user.username = form.username.data
        user.email = form.email.data

        if current_user.is_admin():
            selectedRole = Role.query.filter_by(id=form.role.data.id).first_or_404()

            if len(user.roles) == 0:
                user.roles.append(selectedRole)
            elif user.roles[0] != selectedRole:
                user.roles[0] = selectedRole

        elif current_user.is_PAHSMA():
           if user != current_user:
                selectedRole = Role.query.filter_by(id=4).first_or_404()
                if len(user.roles) == 0:
                    user.roles.append(selectedRole)
                elif user.roles[0] != selectedRole:
                    user.roles[0] = selectedRole

        user.authkey = form.authkey.data

        if(form.password.data):
            user.set_password(form.password.data)

        db.session.commit()
        flash('User Edited')
        return redirect(url_for('index'))

    return render_template('user.html',
        title='Edit User',
        form=form,
        action='Edit',
        user=user,
        current_user=current_user)


@app.route('/user/reset/<username>')
@login_required
def user_reset(username):
    user = User.query.filter_by(username=username).first_or_404()

    msg = Message('Forico planter password reset', recipients = [user.email])
    msg.body = "Hello to reset your password please follow this link\n\n\
    <a href='{}'>Reset password</a> n\
    If you did not reuest this please contact {}".format(app.config['CONTACT_ADDRESS'])
    status = mail.send(msg)

    flash('Send email: {} : {}'.format(user.email, status))

    return redirect(url_for('security.login'))



@app.route('/authkey/check/<key>')
def authkey_check(key):
    u = uuid.UUID(key)
    user = User.query.filter_by(authkey=u).all()
    if user and user[0]:
        j = json.dumps({'username':user[0].username, 'role':'ForicoPlanter-'+user[0].roles[0].name})
        return Response(j, mimetype='application/json')
    else:
        abort(404)


@app.route('/authkey/get')
def authkey_get():
    if current_user.is_authenticated:
        j = json.dumps({
            'User'   :current_user.username,
            'authkey':current_user.authkey.hex,
            'role'   :current_user.roles[0].name})
    else:
        j = json.dumps({'User':None, 'authkey':None })

    return Response(j, mimetype='application/json')


#map page state zero
@app.route('/aglogic')
def aglogic():
    return render_template('aglogic.html')
    abort()

# @app.route('/coupes')
# def coupes():
#     abort()


#map page state zero
@app.route('/map')
def map():
    start_coords = (-41.2665, 146.470)
    folium_map = folium.Map(location=start_coords, zoom_start=7.5)
    control_scale=True,
    # folium_map.save('templates/map.html')
    return folium_map._repr_html_()
    # return render_template('map.html')
  


# #map page after uploaded coupe simulaton
# @app.route('/map_uploadCoupe')
# def map_uploadCoupe():
#     return render_template('map_uploadCoupe.html')
#     abort()


# #adding folium-simulation of coupe uploaded-mapped
# @app.route('/geoTreePlanting')
# def geoTreePlanting():
#     return render_template('geoTreePlanting.html')
#     abort()

# #adding folium-plain map
# @app.route('/geoTreePlanting_noData')
# def geoTreePlanting_noData():
#     return render_template('geoTreePlanting_noData.html')
#     abort()