import babel
import datetime
import json
import math
import os
import re
import subprocess
import uuid
from app import app
from flask_login import current_user



def format_datetime(value, format='medium', tz='UTC'):
    # d = value - datetime.datetime.utcnow()

    tzinfo = babel.dates.get_timezone(tz)

    # return d.total_seconds()
    # if d.total_seconds() > -60*60*24:
    #     return babel.dates.format_timedelta(d, locale='en_AU') + ' ago'
    # else:
    #     return babel.dates.format_datetime(value, format=format, tzinfo=babel.dates.get_timezone('Australia/Hobart'))
    return babel.dates.format_datetime(value, format=format, locale='en_au', tzinfo=tzinfo)


def format_bytes(size, long=False):
    if size is None:
        return None
    size = int(size)
    # 2**10 = 1024
    power = 2**10
    n = 0
    if long:
        power_labels = {0 : '', 1: 'kilo', 2: 'mega', 3: 'giga', 4: 'tera'}
        unit_postfix = 'bytes'
    else:
        power_labels = {0 : '', 1: 'K', 2: 'M', 3: 'G', 4: 'T'}
        unit_postfix = 'B'
    while size > power:
        size /= power
        n += 1
    return size, power_labels[n]+unit_postfix
