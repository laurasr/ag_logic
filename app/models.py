from datetime import datetime
from app import app, db, login, ma
from werkzeug.security import generate_password_hash, check_password_hash
# from flask_login import 
from flask_security import RoleMixin, UserMixin, utils
from sqlalchemy.sql.sqltypes import NullType
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy import BigInteger, Column, Float, Integer, String, text as sa_text, type_coerce
from sqlalchemy.schema import FetchedValue

from sqlalchemy.orm import class_mapper, ColumnProperty

from sqlalchemy.ext.declarative import declared_attr, declarative_base

import geoalchemy2
from geoalchemy2.types import Geometry


from marshmallow_sqlalchemy import ModelConverter
from marshmallow import fields

import json
import uuid
import sys

def str_to_class(classname):
	return getattr(sys.modules[__name__], classname)

class GeometryField(geoalchemy2.Geometry):
	def column_expression(self, col):
		return geoalchemy2.types.func.ST_AsText(col, type_=self)

class Box2D(geoalchemy2.Geometry):
	def column_expression(self, col):
		return geoalchemy2.types.func.ST_AsGeoJSON(col, type_=self)

	def result_processor(self, dialect, coltype):
		def process(value):
			if value is not None:
				try:
					j = json.loads(value)
					ll = j['coordinates'][0][0]
					ur = j['coordinates'][0][2]
					return "[{}, {}, {}, {}]".format(ll[0], ll[1], ur[0],ur[1])
				except:
					return  value
		return process

	# def bind_expression(self, bindvalue):
	# 	print('----------------')
	# 	print(dir(self))
	# 	print('----------------')
	# 	print(dir(bindvalue))
	# 	print('----------------')
	# 	# val = type_coerce(bindvalue, String)
	# 	# print(type(bindvalue))
	# 	# print('TEST BIND EXP: {}'.format(val))
	# 	# xmin = 1;
	# 	# ymin = 1;
	# 	# xmax = 2;
	# 	# ymax = 2;
	# 	return geoalchemy2.types.func.ST_MakeBox2D(geoalchemy2.types.func.ST_Point(bindvalue.xmin, bindvalue.ymin),geoalchemy2.types.func.ST_Point(bindvalue.xmax, bindvalue.ymax))

	def bind_processor(self, dialect):
		def process(bindvalue):
			print('----------------')
			print(dir(self))
			print('----------------')
			print(dir(bindvalue))
			print('----------------')
			print('TEST BIND PRO: {}'.format(bindvalue))
			bindvalue.xmin = 1;
			bindvalue.ymin = 1;
			bindvalue.xmax = 2;
			bindvalue.ymax = 2;
			return bindvalue

	#geoalchemy2.types.func.ST_MakeBox2D 
	#geoalchemy2.types.func.func.ST_Point(xmin, ymin)


class GeoConverter(ModelConverter):
	SQLA_TYPE_MAPPING = ModelConverter.SQLA_TYPE_MAPPING.copy()
	SQLA_TYPE_MAPPING.update({
		Geometry: fields.Str,
		Box2D: fields.Str
	})



@login.user_loader
def load_user(id):
  return User.query.get(int(id))


class BaseModel(db.Model):
	"""Base data model for all objects"""
	__abstract__ = True

	# def __init__(self, *args):
	# 	super().__init__(*args)

	def __repr__(self):
		"""Define a base way to print models"""
		return '%s(%s)' % (self.__class__.__name__, {
			column: value
			for column, value in self._to_dict().items()
		})

	def columns(self):
		"""Return the actual columns of a SQLAlchemy-mapped object"""
		return [prop.key for prop in class_mapper(self.__class__).iterate_properties if isinstance(prop, ColumnProperty)]

	def json(self):
		"""
				Define a base way to jsonify models, dealing with datetime objects
		"""
		return {
			column: value if not isinstance(value, datetime.date) else value.strftime('%Y-%m-%d')
			for column, value in self._to_dict().items()
		}


# defining 'users' TABLE data-model 
class User(UserMixin, BaseModel):
	__tablename__ = 'users'
	# __table_args__ = {'schema': 'web_ui'}
	__table_args__ = {'schema': app.config['BASE_SCH']}

	id               = db.Column(db.Integer, primary_key=True)
	username         = db.Column(db.String(64), index=True, unique=True)
	email            = db.Column(db.String(120), index=True, unique=True)
	password         = db.Column(db.String(128))
	active           = db.Column(db.Boolean())
	authkey          = db.Column(db.String(64))
	current_login_at = db.Column(db.DateTime)
	last_login_at    = db.Column(db.DateTime)
	current_login_ip = db.Column(db.String(64))
	last_login_ip    = db.Column(db.String(64))
	login_count      = db.Column(db.Integer)

	# model_runs = db.relationship('ModelRun', backref='user', lazy='dynamic')
	roles = db.relationship('Role',
		secondary=__table_args__['schema']+'.user_roles',
		backref=db.backref('users', lazy='dynamic'))

	def is_admin(self):
		return self.has_role('Admin')

	def set_password(self, password):
		self.password = utils.hash_password(password)

	def get_security_payload(self):
		self.authkey = uuid.uuid4()
		print(self.authkey)
		return super(self).get_security_payload()



# Define the 'roles' TABLE data-model
class Role(RoleMixin, db.Model):
	__tablename__ = 'roles'
	__table_args__ = {'schema': app.config['BASE_SCH']}

	id = db.Column(db.Integer(), primary_key=True)
	name = db.Column(db.String(50), unique=True)


# Define the 'user_roles' association table
class UserRoles(db.Model):
	__tablename__ = 'user_roles'
	__table_args__ = {'schema': app.config['BASE_SCH']}

	id = db.Column(db.Integer(), primary_key=True)
	user_id = db.Column(db.Integer(),
		db.ForeignKey(User.__table_args__['schema']+'.users.id',
			ondelete='CASCADE'))
	role_id = db.Column(db.Integer(),
		db.ForeignKey(Role.__table_args__['schema']+'.roles.id',
			ondelete='CASCADE'))


# Define the 'companies' TABLE data-model
class Company(db.Model):
	__tablename__ = 'companies'
	__table_args__ = {'schema': app.config['BASE_SCH']}

	id = db.Column(db.Integer(), primary_key=True)
	company_name = db.Column(db.String(), unique=True)
	company_type = db.Column(db.String(), unique=True)


# Define the 'user_companies' association table
class UserCompanies(db.Model):
	__tablename__ = 'user_companies'
	__table_args__ = {'schema': app.config['BASE_SCH']}

	id = db.Column(db.Integer(), primary_key=True)
	user_id = db.Column(db.Integer(),
		db.ForeignKey(User.__table_args__['schema']+'.user.id',
			ondelete='CASCADE'))
	company_id = db.Column(db.Integer(),
		db.ForeignKey(Role.__table_args__['schema']+'.company.id',
			ondelete='CASCADE'))



class PlantingUnit(db.Model):
	__tablename__ = 'coupe_units' #planting_units
	__table_args__ = {'schema':'data'}

	id = db.Column(db.Integer(), primary_key=True)
	geom = db.Column(GeometryField(geometry_type='POLYGON')) #,srid=4326
	coupe_code = db.Column(db.String(254), index=True, unique=True)
	coupe_name = db.Column(db.String(254), index=True, unique=True)
	tree_species = db.Column(db.String(254), index=True, unique=True)
	coupe_seedlot = db.Column(db.String(254), index=True, unique=True)
	target_stems_hectare = db.Column(db.Numeric, index=True, unique=True)
	row_spacing = db.Column(db.Numeric, index=True, unique=True)
	target_stems_hectare = db.Column(db.Numeric, index=True, unique=True)
	interrow_spacing = db.Column(db.Numeric, index=True, unique=True)

	company_id = db.Column(db.Integer(),
		db.ForeignKey(Company.__table_args__['schema']+'.companies.id'))

class Tree(db.Model):
	__tablename__ = 'sample_trees' #trees
	__table_args__ = {'schema': 'data'}

	id = db.Column(db.Integer(), primary_key=True)
	geom = db.Column(GeometryField(geometry_type='POINT', srid=4326))
	planted = db.Column(db.DateTime)


# class sampleTrees(db.Model):
# 	__tablename__ = 'sample_trees'
# 	__table_args__ = {'schema': 'data'}

# 	id = db.Column(db.Integer(), primary_key=True)
# 	geom = db.Column(GeometryField(geometry_type='POLYGON')) #,srid=4326
# 	name = db.Column(db.String(), unique=True)

# 	company_id = db.Column(db.Integer(),
# 		db.ForeignKey(Company.__table_args__['schema']+'.companies.id'))
