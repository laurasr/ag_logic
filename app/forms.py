from flask_wtf import FlaskForm
from wtforms import HiddenField, DecimalField, StringField, PasswordField, BooleanField, SubmitField, MultipleFileField, FileField, TextAreaField, IntegerField, RadioField, SelectField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, Optional, InputRequired, UUID, Regexp
from wtforms.ext.sqlalchemy.fields import QuerySelectField

from flask_security.forms import LoginForm

from app.models import User, Role
from app import app

import safe


def roles():
    return Role.query


class ExtendedLoginForm(LoginForm):
    email = StringField('Username or Email Address', [InputRequired()])
    password = PasswordField('Password', validators=[])


class UserForm(FlaskForm):
    id = HiddenField("user_id")
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()], render_kw={'class':'myclass','style':'font-size:150%'})
    password = PasswordField('Password', validators=[Optional()], render_kw={'class':'myclass'})
    password2 = PasswordField('Repeat Password', validators=[Optional(), EqualTo('password')])
    
    role = QuerySelectField(query_factory=roles,
        allow_blank=False, get_label='name', validators=[Optional()])

    authkey = StringField('authkey', validators=[Optional(), UUID()])

    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()

        if user is not None and int(user.id) != int(self.id.data):
            raise ValidationError('Please use a different username.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None and int(user.id) != int(self.id.data):
            raise ValidationError('Please use a different email address.')

    def validate_password(self, password):
        strength = safe.check(password.data, length=app.config['SECURITY_PASSWORD_MIN_LENGTH'])

        if not strength.valid:
            raise ValidationError('Password is not strong enough - {}'.format(strength.message))

class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')