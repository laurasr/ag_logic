from app import app, db, mail
from app.models import User, Role, db

app.config['DEBUG'] = True


@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Role': Role}


if __name__ == "__main__":
	app.run()
    

