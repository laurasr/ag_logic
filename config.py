import os 

class Config(object):
	#FLASK SETTINGS
	TESTING = True
	DEBUG = True
	#SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
	SECRET_KEY = 'KbroiwIY+aKE7W5W/uQMdq4JNMWJgdRXYAQ/lUYbTmE='

	BASE_SCH = 'web_ui'
	SQLALCHEMY_DATABASE_URI = 'postgresql://localhost:5432/forico'
	SQLALCHEMY_TRACK_MODIFICATIONS = False

		
	#SECURITY SETTINGS
	SECURITY_USER_IDENTITY_ATTRIBUTES = ('username','email')
	SECURITY_PASSWORD_HASH = 'bcrypt'
	SECURITY_PASSWORD_SALT = 'bO88tc+NNG337RZI/Qo34Lg1d/n14jCoP/VxtFjC230='
	SECURITY_PASSWORD_MIN_LENGTH = 8
	SECURITY_TRACKABLE = True

	#EMAIL SETTINGS
	MAIL_SERVER = 'smtp.eskmapping.com.au'
	MAIL_PORT = 465
	MAIL_USERNAME = 'support@eskmapping.com.au'
	MAIL_PASSWORD = 'yypjramnsdjovjra'
	MAIL_USE_TLS = False
	MAIL_USE_SSL = True
	MAIL_DEFAULT_SENDER = 'support@eskmapping.com.au'

	MAIL_DEBUG = True
	MAIL_SUPPRESS_SEND = False


	#Database SETTINGS
	# POSTGRES_CON = {
	# 	# 	'user': 'forico_planter',
	# 	# 	'pw'  : 'forico_planter',
	# 		'db'  : 'forico',
	# 	 	'host': 'localhost',
	# 	 	'port': '5432',
	# 	 }


	#SQLALCHEMY_DATABASE_URI = 'postgresql://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s'#% POSTGRES_CON

